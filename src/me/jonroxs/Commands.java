package me.jonroxs;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;

public class Commands implements CommandExecutor {
	private static Main plugin;

	public Commands(Main plugin) {
		Commands.plugin = plugin;
	}

	public Permission playerHealPermission = new Permission("playerHeal.allowed");

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (cmd.getName().equalsIgnoreCase("healplayer") && sender instanceof Player) {

			Player player = (Player) sender;

			if (!(player.hasPermission("playerHeal.allowed"))) {

				player.sendMessage(ChatColor.RED + "You do not have permission to use this command!");

			}

			if (player.hasPermission("playerHeal.allowed")) {
				// Arg 0 = first word entered after command, Arg 2 = second word
				// after command, ect.
				int length = args.length;

				if (length == 1) {

					boolean playerFound = false;

					for (Player playerToHeal : Bukkit.getServer().getOnlinePlayers()) {
						if (playerToHeal.getName().equalsIgnoreCase(args[0])) {
							playerToHeal.setHealth((int) 20.0);
							playerToHeal.sendMessage(ChatColor.YELLOW + "You have been healed by " + player.getName() + "!");
							player.sendMessage(ChatColor.YELLOW + playerToHeal.getName() + " has been healed sucessfully!");
							playerFound = true;
							break;
						}
					}

					if (playerFound == false) {
						player.sendMessage(ChatColor.RED + args[0] + " was not found!");
					}

				} else
					player.sendMessage(ChatColor.RED + "Incorrect arguments");

				return true;
			}

			return false;

		}
		if (cmd.getName().equalsIgnoreCase("zombiekills") && sender instanceof Player) {

			Player player = (Player) sender;

			player.sendMessage(ChatColor.GOLD + "" + plugin.getConfig().getInt("ZombieKills" + " Zombies have been killed!"));

			return true;
		}
		if (cmd.getName().equalsIgnoreCase("totalexp") && sender instanceof Player) {

			Player player = (Player) sender;

			player.sendMessage(ChatColor.GOLD + "Total Exp: " + player.getTotalExperience());

			return true;

		} else if (cmd.getName().equalsIgnoreCase("exp") && sender instanceof Player) {

			Player player = (Player) sender;

			player.sendMessage(ChatColor.GOLD + "EXP to next level: " + player.getExpToLevel());

			return true;
		}
		return false;

	}

}