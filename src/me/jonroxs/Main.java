package me.jonroxs;


import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	@Override
	public void onDisable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		System.out.println(pdfFile.getName() + " has been disabled");
	}

	@Override
	public void onEnable() {
		this.getConfig().addDefault("ZombieKills", 0);
		this.getConfig().options().copyDefaults(true);
		saveConfig();
		this.getCommand("zombiekills").setExecutor(new Commands(this));
		this.getCommand("totalexp").setExecutor(new Commands(this));
		this.getCommand("exp").setExecutor(new Commands(this));
		this.getCommand("healplayer").setExecutor(new Commands(this));
		PluginDescriptionFile pdfFile = this.getDescription();
		System.out.println(pdfFile.getName() + " Version " + pdfFile.getVersion() + " has been enabled");
		new EventListener(this);
		
	}

}
