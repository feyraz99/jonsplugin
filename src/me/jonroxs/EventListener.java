package me.jonroxs;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

public class EventListener implements Listener {

	public Main plugin;

	public EventListener(Main plugin) {
		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {

		Player player = e.getPlayer();

		e.setJoinMessage(ChatColor.YELLOW + "Welcome, " + player.getName() + " to Atomic Voltz! Please vote for the server and recieve cool items and money by doing /vote!");

		// If a new player
		if (player.hasPlayedBefore() == false) {
			player.sendMessage(ChatColor.GREEN + "As you are new to the server, have a free apple!");

			// Adds one apple to the player inventory
			player.getInventory().addItem(new ItemStack(Material.APPLE, 1));

		}

	}

	@EventHandler
	public void killZombie(EntityDeathEvent e) {

		// Entity killed
		Entity deadEntity = e.getEntity();
		// Entity Killer
		Entity killer = e.getEntity().getKiller();

		if (killer instanceof Player && deadEntity instanceof Zombie) {

			Player player = (Player) killer;

			int killCount = plugin.getConfig().getInt("ZombieKills");

			// Add one kill to the counter
			plugin.getConfig().set("ZombieKills", killCount + 1);

			player.sendMessage(ChatColor.GREEN + "You have killed a zombie, well done!");

		}

	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {

		Player player = e.getPlayer();

		if (e.getBlock().getType() == Material.BEDROCK) {
			if (!player.hasPermission("playerAbilities.allowed")) {
				player.sendMessage(ChatColor.RED + "You cannot place " + e.getBlock().getType().toString());
				e.setCancelled(true);
			} else
				player.sendMessage(ChatColor.YELLOW + "You are allowed to place " + e.getBlock().getType().toString());

		}

	}
	
	@EventHandler
	public void onBlockPlace2(BlockPlaceEvent e) {
		
		
	}
}
